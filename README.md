![App Icon Requests](https://gitlab.gnome.org/Teams/Design/app-icon-requests/-/raw/master/_assets/hero.png?ref_type=heads)

# App Icon Requests

This module is the way to request an app icon designed by one of GNOME's visual designers. You may also find some assets useful if you're rather not wait and kit bashed it yourself. There's a good primer on how to design an app icon below.

## Requests

1. [File an issue](https://gitlab.gnome.org/Teams/Design/app-icon-requests/-/issues/new?issuable_template=request). There's a much higher chance of getting your icon designed, if your app strives to follow the [GNOME human interface guidelines](https://developer.gnome.org/hig/), particularly in the [app naming](https://developer.gnome.org/hig/guidelines/app-naming.html) aspect.
2. Get involved early. Use the comment section in the issue providing early feedback or join our [matrix channel](https://matrix.to/#/#appicondesign:gnome.org) for realtime discourse.
3. Ship the source along the exported assets. Very rarely does the app identity not evolve, and having the source plate ship with your icon helps future designers (such as making use of alternative variants that aren't exported).

## Designing yourself

[Tobias' icon design tutorial](https://blogs.gnome.org/tbernard/2019/12/30/designing-an-icon-for-your-app/) gives you a quick introduction into the process of creating an app icon using our suggested toolchain. You can find more details on how to design the symbolic variant in [Jakub's blog post](https://blog.jimmac.eu/2021/how-to-symbolic/). The design system, mainly defined by Lapo Calamandrei, uses a unique perspective, see [the HIG](https://developer.gnome.org/hig/guidelines/app-icons.html) for more details.
